#### Get Code

git clone git@gitlab.com:dairejpwalsh/PhD_CS401_Project

#### Running Code

Process drone data with

python segemter_drone.py

Process van data with

python segemter_van.py

#### Data

Data used in project is available on request. Due to size(Multiple GB's) this
data was not pushed to the repository

#### Software
All recommended software is opensource.

Qgis is recommended for viewing ShapeFiles.

https://qgis.org/en/site/

Add Openlayers Plugin for good basemaps

https://plugins.qgis.org/plugins/openlayers_plugin/

Cloudcomapre recommended for viewing LAS files

http://www.danielgm.net/cc/
