import json
import subprocess
import os.path
import os
import csv
import pandas as pd
import numpy as np
from pyproj import Proj, transform
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from matplotlib.colors import ListedColormap
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from datetime import datetime

class Segmenter(object):
    def __init__(self, points, src_epsg, dst_epsg, src_path):
        # Set segmenter points
        # Source and destination EPSG's
        # Source path to file to crop/segment
        self.points = points
        self.src_epsg = src_epsg
        self.dst_epsg = dst_epsg
        self.src_path = src_path

    def create_polygon(self):
        # Create WKT Polygon String from list of points
        polygon = "POLYGON(("
        count = 0

        for newPoint in self.points[0]:

            if count % 10 == 0:
                inProj = Proj(init=self.src_epsg)
                outProj = Proj(init=self.dst_epsg)

                x1, y1 = newPoint[0], newPoint[1]
                x2, y2 = transform(inProj,
                                   outProj,
                                   x1,
                                   y1)

                polygon += str(x2) + " " + str(y2) + ","

            count += 1

        inProj = Proj(init=self.src_epsg)
        outProj = Proj(init=self.dst_epsg)

        x1, y1 = self.points[0][0][0], self.points[0][0][1]
        x2, y2 = transform(inProj,
                           outProj,
                           x1,
                           y1)

        polygon += str(x2) + " " + str(y2) + ","
        polygon = polygon[:-1]
        polygon += "))"
        # Return WKT Polygon string
        return polygon

    def segment_in(self, out_name):
        # Segement Data Using PDAL Pipeline, update paths as needed
        polygon = self.create_polygon()
        pdal_Command = ("docker run -v /home/daire/Code/CS401_Projects/Project:/data " +
                        "pdal/pdal:1.5 pdal pipeline " +
                        "data/assets/pipelines/in.json " +
                        "--readers.las.filename=data/" +
                        self.src_path + " "
                        "--filters.crop.polygon=\"" + polygon + "\" " +
                        "--writers.las.filename=data/scratch/" +
                        out_name)
        # Subrprocess.call caused error???
        # os.system didn't
        os.system(pdal_Command)

    def segment_out(self, out_name):
        # Segement Data Using PDAL Pipeline, update paths as needed
        polygon = self.create_polygon()
        pdal_Command = ("docker run -v /home/daire/Code/CS401_Projects/Project:/data " +
                        "pdal/pdal:1.5 pdal pipeline " +
                        "data/assets/pipelines/out.json " +
                        "--readers.las.filename=data/" +
                        self.src_path + " "
                        "--filters.crop.polygon=\"" + polygon + "\" " +
                        "--writers.las.filename=data/scratch/" +
                        out_name)
        # Subrprocess.call caused error???
        # os.system didn't
        os.system(pdal_Command)

    def subsample(self, out_name):
        # Subsample Data Using PDAL Pipeline, update paths as needed
        polygon = self.create_polygon()
        pdal_Command = ("docker run -v /home/daire/Code/CS401_Projects/Project:/data " +
                        "pdal/pdal:1.5 pdal pipeline " +
                        "data/assets/pipelines/subsample.json " +
                        "--readers.las.filename=data/" +
                        self.src_path + " "
                        "--writers.las.filename=data/scratch/" +
                        out_name)
        os.system(pdal_Command)
        return out_name


class Data_Preprocessor(object):

    def __init__(self, src_epsg, dst_epsg, scratch_path):
        # Set Source and destination EPSG's
        # Source path to file to crop/segment
        self.src_epsg = src_epsg
        self.dst_epsg = dst_epsg
        self.src_path = scratch_path

    def las2txt(self, src_path, dst_path=None):
        # Converts las file to CSV using las2text
        # Update paths as needed
        if dst_path is None:
            dst_path = src_path[:-3] + "txt"

        cmd = ("las2txt " +
               "-i " +
               src_path + " " +
               "--parse xyzRGB " +
               "--delimiter , " +
               "--labels " +
               "--header " +
               "-o " +
               dst_path)

        if not os.path.exists(dst_path):
            print("Got here")
            subprocess.call(cmd.split(" "))
            return dst_path
        else:
            print("File Already Excists")
            return dst_path

    def split_data(self):
        # Not enough time to fully implement class
        print("Datasplitter")


if __name__ == "__main__":
    # Open road edge GeoJSON file
    with open('assets/road_edge.geojson') as data_file:
        data = json.load(data_file)

    # Get just coordinates
    road_edge_coords = data["features"][0]["geometry"]["coordinates"]

    # Set filenames for cropped road files
    road_points = "drone_road.las"
    bank_points = "drone_bank.las"

    # Create Segmenter Object
    my_segmenter = Segmenter(road_edge_coords,
                             "epsg:3857",
                             "epsg:2157",
                             "scratch/drone_segmented_road.las")
    # subsample las files
    new_path = "scratch/" + my_segmenter.subsample("drone_subsample.las")
    my_segmenter.src_path = new_path

    # Segement out road and bank points
    my_segmenter.segment_in(road_points)
    my_segmenter.segment_out(bank_points)

    # Initiate Preprocessor object
    my_preprocessor = Data_Preprocessor("EPSG:2157",
                                        "EPSG:3857",
                                        "/home/daire/Code/CS401_Projects/Project/scratch")

    # Convert processed LAS files to CSV's
    road_points = my_preprocessor.las2txt("scratch/drone_road.las")
    bank_points = my_preprocessor.las2txt("scratch/drone_bank.las")

    ###########################################################################
    #  Partitioning Data                                                     #
    ###########################################################################
    # Assign Road Lables
    df_road = pd.read_csv(road_points)
    df_road["class"] = 1
    # Assign Bank Lables
    df_bank = pd.read_csv(bank_points)
    df_bank["class"] = 0
    # Recombnine in 1 dataset
    df_combined = pd.concat([df_road, df_bank])
    # Extract Data only needed for training and testing
    X  = df_combined.loc[:, 'Z':'Blue']
    y = df_combined.loc[:, "class"]
    # Split data
    X_train, X_test, y_train, y_test = train_test_split(X,
                                                        y,
                                                        test_size=0.3,
                                                        random_state=1,
                                                        stratify=y)

    ###########################################################################
    #  Scaling Data                                                           #
    ###########################################################################
    # Fit the StandardScaler class only once on the training data and use those
    # parameters to transform the test set or any new data point.
    stdsc = StandardScaler()
    X_train_std = stdsc.fit_transform(X_train)
    X_test_std = stdsc.transform(X_test)
    ###########################################################################
    #  Dimensionality reduction                                               #
    ###########################################################################
    """from sklearn.decomposition import PCA as sklearnPCA
    sklearn_pca = sklearnPCA(n_components=2)
    X_train_pca = sklearn_pca.fit_transform(X_train_std)

    print(X_train_pca)"""
    ###########################################################################                                              #
    #  Random Forest took :  1:56:41.312297
    # 0.992756799633
    # {'bootstrap': False, 'min_samples_leaf': 10, 'n_estimators': 50, 'criterion': 'entropy', 'min_samples_split': 2, 'max_depth': None}
    ###########################################################################
    from sklearn.ensemble import RandomForestClassifier
    # Uncomment to train data for best fitting params
    """
    param_grid = {"n_estimators" :[10, 50, 100],
                  "max_depth":[3, 10,  None],
                  "min_samples_split": [2, 3, 10, 30],
                  "min_samples_leaf": [1, 3, 10],
                  "bootstrap": [True, False],
                  "criterion": ["gini", "entropy"]}


    random_forest = RandomForestClassifier(random_state=1)

    startTime = datetime.now()

    grid_search_rm = GridSearchCV(random_forest,
                               param_grid,
                               n_jobs=-1,
                               cv=10,
                               scoring='accuracy',
                               verbose=10)

    grid_search_RandomForest = grid_search_rm.fit(X_train_std, y_train)

    print("Random Forest took :  " +  str(datetime.now() - startTime))
    print(grid_search_RandomForest.best_score_)

    print(grid_search_RandomForest.best_params_)
    """
    random_forest = RandomForestClassifier(random_state=1,
                                           bootstrap=False,
                                           min_samples_leaf=10,
                                           n_estimators=50,
                                           criterion="entropy",
                                           min_samples_split=2,
                                           max_depth=None)
    rn_model = random_forest.fit(X_train_std, y_train)
    startTime = datetime.now()
    result = rn_model.predict(X_test_std)
    print("Random Forest took :  " +  str(datetime.now() - startTime))
    bad_predicts = (y_test != result).sum()
    percent_bad = (bad_predicts/len(X_test_std))*100.0
    x_test_index = X_test.index.tolist()

    print('Misclassified samples: ' + str(bad_predicts))
    print('Misclassified percent: ' + str(percent_bad))
    print(result)
    count = 0
    predicted_road_list = []
    # For predicted road points retrieve all origional values
    for count, elem in enumerate(result):
        if elem == 1:
            good_index = x_test_index[count]
            good_points  =  df_combined.iloc[good_index].values.tolist()[:-1]
            predicted_road_list.append(good_points)

    # Write data to CSV
    with open('output/drone_random_forest.csv', 'w') as myfile:
        writer = csv.writer(myfile)
        writer.writerows(predicted_road_list)
    # Convert back to LAS
    cmd = ("txt2las " +
           "-i " +
           "output/drone_random_forest.csv " +
           "--parse xyzRGB " +
           "-o " +
           "output/drone_random_forest.las")
    subprocess.call(cmd.split(" "))
    # Perform SOR with CloudCompare
    cmd = ("CloudCompare -O /home/daire/Code/CS401_Projects/Project/output/drone_random_forest.las -SOR 7 0.01")
    subprocess.call(cmd.split(" "))
    print("\n\n\nNext Classifier\n\n\n")
    ###########################################################################
    # SVM took :  1:24:13.895469
    # 0.991525246832
    # {'kernel': 'rbf', 'C': 100.0}                                                            #
    ###########################################################################
    # Uncomment to train data for best fitting params
    from sklearn.svm import SVC
    """
    pipe_svc = SVC(random_state=1,
                   verbose=True)
    param_range = [0.001, 0.01, 1.0, 10.0, 100.0]
    param_grid = [
                  {
                   'C': param_range,
                   'kernel': ['linear']
                   }, {
                    'C': param_range,
                    'kernel': ['rbf']
                    }
                  ]

    grid_search = GridSearchCV(pipe_svc,
                               param_grid,
                               scoring='accuracy',
                               cv=10,
                               n_jobs=-1,
                               verbose=10)
    print("Fitting SVM")
    print(param_range)
    startTime = datetime.now()
    grid_search_svm = grid_search.fit(X_train_std, y_train)
    print("SVM took :  " + str(datetime.now() - startTime))
    print(grid_search_svm.best_score_)

    print(grid_search_svm.best_params_)
    """
    svc_class = SVC(random_state=1,
                    kernel="rbf",
                    C=100)
    svc_model = svc_class.fit(X_train_std, y_train)
    startTime = datetime.now()
    result = svc_model.predict(X_test_std)
    print("Support Vector took :  " +  str(datetime.now() - startTime))
    bad_predicts = (y_test != result).sum()
    percent_bad = (bad_predicts/len(X_test_std))*100.0
    x_test_index = X_test.index.tolist()

    print('Misclassified samples: ' + str(bad_predicts))
    print('Misclassified percent: ' + str(percent_bad))
    print(result)
    count = 0
    predicted_road_list = []
    # For predicted road points retrieve all origional values
    for count, elem in enumerate(result):
        if elem == 1:
            good_index = x_test_index[count]
            good_points  =  df_combined.iloc[good_index].values.tolist()[:-1]
            predicted_road_list.append(good_points)

    # Write data to CSV
    with open('output/drone_support_vector.csv', 'w') as myfile:
        writer = csv.writer(myfile)
        writer.writerows(predicted_road_list)
    # Convert back to LAS
    cmd = ("txt2las " +
           "-i " +
           "output/drone_support_vector.csv " +
           "--parse xyzRGB " +
           "-o " +
           "output/drone_support_vector.las")
    subprocess.call(cmd.split(" "))
    # Perform SOR with CloudCompare
    cmd = ("CloudCompare -O /home/daire/Code/CS401_Projects/Project/output/drone_support_vector.las -SOR 7 0.01")
    subprocess.call(cmd.split(" "))
    print("\n\n\nNext Classifier\n\n\n")
    ###########################################################################                                                         #
    # MLP took :  1:08:47.404721
    # 0.992144502195
    # {'alpha': 0.0001, 'activation': 'relu', 'learning_rate': 'constant', 'hidden_layer_sizes': (30, 40, 50)}
    ###########################################################################
    # Uncomment to train data for best fitting params
    from sklearn.neural_network import MLPClassifier
    """

    mlp = MLPClassifier(max_iter=500, random_state=1)#, alpha=0.1, activation="relu", learning_rate="constant", hidden_layer_sizes=(40, 40, 40))

    parameters = {
                'learning_rate': ["constant", "invscaling", "adaptive"],
                'hidden_layer_sizes': [(30, 30, 30),
                                       (40, 40, 40),
                                       (50, 50, 50),
                                       (50, 40, 30),
                                       (30, 40, 50),
                                       (30, 20, 30)],
                'alpha': [1.0e-01,
                          1.0e-02,
                          1.0e-03,
                          1.0e-04],
                'activation': ["logistic", "relu", "tanh"]
                }
    gs = GridSearchCV(estimator=mlp,
                      param_grid=parameters,
                      n_jobs=-1,
                      verbose=10)
    print("Fitting MLP")
    print(parameters)
    startTime = datetime.now()
    gs = gs.fit(X_train_std, y_train)
    print("MLP took :  " + str(datetime.now() - startTime))
    print(gs.best_score_)
    print(gs.best_params_)
    """
    mlp_class = MLPClassifier(max_iter=500,
                              random_state=1,
                              alpha=0.0001,
                              activation="relu",
                              learning_rate="constant",
                              hidden_layer_sizes=(30, 40, 50))
    mlp_model = mlp_class.fit(X_train_std, y_train)
    startTime = datetime.now()
    result = mlp_model.predict(X_test_std)
    print("MLP took :  " +  str(datetime.now() - startTime))
    bad_predicts = (y_test != result).sum()
    percent_bad = (bad_predicts/len(X_test_std))*100.0
    x_test_index = X_test.index.tolist()

    print('Misclassified samples: ' + str(bad_predicts))
    print('Misclassified percent: ' + str(percent_bad))
    print(result)
    count = 0
    predicted_road_list = []
    # For predicted road points retrieve all origional values
    for count, elem in enumerate(result):
        if elem == 1:
            good_index = x_test_index[count]
            good_points  =  df_combined.iloc[good_index].values.tolist()[:-1]
            predicted_road_list.append(good_points)

    # Write data to CSV
    with open('output/drone_mlp.csv', 'w') as myfile:
        writer = csv.writer(myfile)
        writer.writerows(predicted_road_list)
    # Convert back to LAS
    cmd = ("txt2las " +
           "-i " +
           "output/drone_mlp.csv " +
           "--parse xyzRGB " +
           "-o " +
           "output/drone_mlp.las")
    subprocess.call(cmd.split(" "))
    # Perform SOR with CloudCompare
    cmd = ("CloudCompare -O /home/daire/Code/CS401_Projects/Project/output/drone_mlp.las -SOR 7 0.01")
    subprocess.call(cmd.split(" "))
    print("\n\n\nNext Classifier\n\n\n")
